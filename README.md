# ClimBook
Projekt Beschreibung

## Inhaltsverzeichnis
1. [GettingStarted](#getting-started)
2. [Datenbank](#datenbank)
3. [Tests](#tests)
4. [URL's](#urls)
5. [Entwickler](#authors)


<a name="getting-started"></a>
### Getting Started
Anleitung wie man sich eine Kopie des Projektes holt und lokal laufen lässt.

#### Voraussetzung
Um das Projekt zu installieren benötigt man die folgenden Git Repositories:

* [Backend](https://gitlab.com/Gerozier/climbook.git)

Zusätzlich gibt es in einem eigenem Repository ein Frontend:
* [Frontend](https://gitlab.com/Gerozier/climbook_fe.git)

#### Installieren
Backend:
```
cd  c: ./climbook
cd mvn clean install
mvn exec:java -Dexec.mainClass="ClimbookApplication"
```

<a name="datenbank"></a>
### Datenbank
Beim Start des Programms wird automatisch eine MariaDB Tabelle erstellt (sofern die Datenbank gestartet ist) mit dem Namen "climbook".
Diese wird mit Daten aus der data.sql Datei im ressources Ordner vorbefüllt.

<a name="tests"></a>
### Tests
```
cd  c: ./climbook
cd mvn test
```

<a name="urls"></a>
### URL's
Wenn das Backend läuft können über Swagger die Schnittstellen getestet werden unter folgendem Link:

Swagger - http://localhost:8080/swagger-ui.html

<a name="authors"></a>
### Entwickler
* Dennis Brämer [Gitlab Profil: Gerozier](https://gitlab.com/Gerozier)
* Mareike Emde [Gitlab Profil: Moinamour](https://gitlab.com/moinamour)
