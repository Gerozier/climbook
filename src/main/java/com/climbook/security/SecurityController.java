package com.climbook.security;

import com.climbook.dtos.LoginDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {

    @GetMapping("/api/v1/sessionuser")
    public ResponseEntity<LoginDTO> sessionuser(@AuthenticationPrincipal UserDetails userDetails) {
        if(userDetails==null){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.ok(LoginDTO.builder()
                .role(userDetails.getAuthorities().toString())
                .username(userDetails.getUsername())
                .build()
        );
    }
}
