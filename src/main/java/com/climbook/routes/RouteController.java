package com.climbook.routes;

import com.climbook.dtos.RouteDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RouteController {

    public static final String PATH_API_ROCK_ROUTES_ROCK_CLIENT_ID = "/api/v1/rock/routes/{rockClientId}";
    public static final String PATH_API_ROUTES_CLIENT_ID = "/api/v1/routes/{clientId}";

    private final RouteService routeService;

    public RouteController(RouteService routeService) {
        this.routeService = routeService;
    }

    @GetMapping(PATH_API_ROCK_ROUTES_ROCK_CLIENT_ID)
    public ResponseEntity<List<RouteDTO>> getAllRoutesByRockClientId(@PathVariable String rockClientId, @AuthenticationPrincipal UserDetails userDetails) {
        List<RouteDTO> allRoutesByRockClientId = routeService.getAllRoutesByRockClientId(rockClientId, userDetails);
        if (allRoutesByRockClientId.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.NO_CONTENT)
                    .body(allRoutesByRockClientId);
        }
        return ResponseEntity.ok(allRoutesByRockClientId);
    }

    @GetMapping(PATH_API_ROUTES_CLIENT_ID)
    public ResponseEntity<RouteDTO> getRouteByClientID(@PathVariable String clientId, @AuthenticationPrincipal UserDetails userDetails) {
        RouteDTO routeByClientId = routeService.getOneRoute(clientId, userDetails);
        if (routeByClientId == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(routeByClientId);
    }

    @PostMapping(PATH_API_ROCK_ROUTES_ROCK_CLIENT_ID)
    public ResponseEntity<HttpStatus> createRoute(@RequestBody Route route, @PathVariable String rockClientId) {
        if (route.getName().equals("") || route.getName() == null) {
            return ResponseEntity.badRequest().build();
        }
        routeService.createRoute(route, rockClientId);
        return ResponseEntity.ok().build();
    }

    @PutMapping(PATH_API_ROUTES_CLIENT_ID)
    public ResponseEntity<HttpStatus> updateRoute(@RequestBody Route route, @PathVariable String clientId) {
        if (route.getName().equals("") || route.getName() == null) {
            return ResponseEntity.badRequest().build();
        }
        HttpStatus httpStatus = routeService.updateRoute(route, clientId);
        if (httpStatus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(PATH_API_ROUTES_CLIENT_ID)
    public ResponseEntity<HttpStatus> deleteRoute(@PathVariable String clientId) {
        HttpStatus httpStatus = routeService.deleteRoute(clientId);
        if (httpStatus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }


    @PostMapping(PATH_API_ROUTES_CLIENT_ID)
    public ResponseEntity<HttpStatus> saveRouteToUser(@PathVariable String clientId, @AuthenticationPrincipal UserDetails userDetails) {
        HttpStatus httpStatus = routeService.setOrDeleteRouteFromUserList(clientId, userDetails);
        if (httpStatus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/v1/unregistered/routes/{clientId}")
    public ResponseEntity<RouteDTO> getRouteByClientIDUnregistered(@PathVariable String clientId, @AuthenticationPrincipal UserDetails userDetails) {
        RouteDTO routeByClientId = routeService.getOneRoute(clientId, userDetails);
        if (routeByClientId == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(routeByClientId);
    }

    @GetMapping("/api/v1/unregistered/rock/routes/{rockClientId}")
    public ResponseEntity<List<RouteDTO>> getAllRoutesByRockClientIdUnregistered(@PathVariable String rockClientId, @AuthenticationPrincipal UserDetails userDetails) {
        List<RouteDTO> allRoutesByRockClientId = routeService.getAllRoutesByRockClientId(rockClientId, userDetails);
        if (allRoutesByRockClientId.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.NO_CONTENT)
                    .body(allRoutesByRockClientId);
        }
        return ResponseEntity.ok(allRoutesByRockClientId);
    }


}









