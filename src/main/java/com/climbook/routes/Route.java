package com.climbook.routes;

import com.climbook.rocks.Rock;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@AllArgsConstructor
@Builder
public class Route {

    @Id
    @JsonIgnore
    private String routeId;

    private String clientId;

    private String name;
    private String description;
    private String difficulty;
    private long createDate;
    private long lastUpdate;
    private long addedByUser;

    @ManyToOne
    private Rock rock;

    public Route() {
    }
}
