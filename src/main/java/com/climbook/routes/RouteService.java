package com.climbook.routes;

import com.climbook.Utils;
import com.climbook.dtos.RouteDTO;
import com.climbook.rocks.RockRepository;
import com.climbook.user.User;
import com.climbook.user.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Service
public class RouteService {

    private final RouteRepository routeRepository;
    private final RockRepository rockRepository;
    private final UserRepository userRepository;

    public RouteService(RouteRepository routeRepository, RockRepository rockRepository, UserRepository userRepository) {
        this.routeRepository = routeRepository;
        this.rockRepository = rockRepository;
        this.userRepository = userRepository;
    }

    public RouteDTO buildRouteDTO(Route route) {
        return RouteDTO.builder()
                .clientId(route.getClientId())
                .description(route.getDescription())
                .name(route.getName())
                .difficulty(route.getDifficulty())
                .createDate(route.getCreateDate())
                .lastUpdate(route.getLastUpdate())
                .complete(false)
                .build();
    }

    private void actualizeUsersRouteList(User byUsername, List<Route> routesByUser) {
        byUsername.setRoutes(routesByUser);
        userRepository.save(byUsername);
    }

    public void createRoute(Route route, String rockClientId) {
        if (route.getDifficulty().equals("") || route.getDifficulty() == null) {
            route.setDifficulty("n.A.");
        }
        routeRepository.save(new Route.RouteBuilder()
                .rock(rockRepository.findRockByClientId(rockClientId))
                .routeId(UUID.randomUUID().toString())
                .clientId(UUID.randomUUID().toString())
                .description(route.getDescription())
                .name(route.getName())
                .difficulty(route.getDifficulty())
                .createDate(Utils.getCurrentMillis())
                .lastUpdate(Utils.getCurrentMillis())
                .build());
    }

    public List<RouteDTO> getAllRoutesByRockClientId(String rockClientId, UserDetails userDetails) {
        List<Route> routesByRockClientId = routeRepository.findRoutesByRock_ClientId(rockClientId);
        routesByRockClientId.sort(Comparator.comparing(Route::getName));
        if (userDetails == null) {
            List<RouteDTO> unregisteredUserList = new ArrayList<>();
            unregisteredUserList.add(buildRouteDTO(routesByRockClientId.get(0)));
            return unregisteredUserList;
        }

        List<RouteDTO> registeredUserList = new ArrayList<>();
        routesByRockClientId.forEach(route -> {
            RouteDTO routeDTO = buildRouteDTO(route);
            registeredUserList.add(routeDTO);
        });
        User byUsername = userRepository.findByUsername(userDetails.getUsername());
        List<Route> routes = byUsername.getRoutes();
        routes.forEach(route -> registeredUserList.forEach(rockdto -> {
            if (route.getName().equals(rockdto.getName())) {
                rockdto.setComplete(true);
            }
        }));

        return registeredUserList;
    }

    public RouteDTO getOneRoute(String clientId, UserDetails userDetails) {
        Route routeByClientId = routeRepository.findRouteByClientId(clientId);
        RouteDTO routeDTO = buildRouteDTO(routeByClientId);
        if (userDetails == null) {
            return routeDTO;
        }
        User byUsername = userRepository.findByUsername(userDetails.getUsername());
        List<Route> routes = byUsername.getRoutes();
        routes.forEach(route -> {
            if (route.getName().equals(routeDTO.getName())) {
                routeDTO.setComplete(true);
            }
        });
        return routeDTO;
    }

    public HttpStatus updateRoute(Route route, String clientId) {
        Route routeToUpdateByClientId = routeRepository.findRouteByClientId(clientId);
        if (routeToUpdateByClientId == null) {
            return null;
        }

        routeToUpdateByClientId.setName(route.getName());
        routeToUpdateByClientId.setDescription(route.getDescription());
        routeToUpdateByClientId.setLastUpdate(Utils.getCurrentMillis());

        if (route.getDifficulty().equals("") || route.getDifficulty() == null) {
            routeToUpdateByClientId.setDifficulty("n.A.");
        } else {
            routeToUpdateByClientId.setDifficulty(route.getDifficulty());
        }
        routeRepository.save(routeToUpdateByClientId);

        return HttpStatus.OK;
    }

    public HttpStatus deleteRoute(String clientId) {
        Route routeToDelete = routeRepository.findRouteByClientId(clientId);
        if (routeToDelete == null) {
            return null;
        }

        routeRepository.delete(routeToDelete);

        return HttpStatus.OK;
    }

    public HttpStatus setOrDeleteRouteFromUserList(String clientId, UserDetails userDetails) {
        Route routeByClientId = routeRepository.findRouteByClientId(clientId);
        if (routeByClientId == null) {
            return null;
        }
        User byUsername = userRepository.findByUsername(userDetails.getUsername());
        List<Route> routesByUser = byUsername.getRoutes();
        Route containedRoute = routesByUser.stream()
                .filter(route -> route.getClientId().equals(clientId))
                .findAny()
                .orElse(null);
        if (containedRoute != null) {
            routesByUser.remove(containedRoute);
            actualizeUsersRouteList(byUsername, routesByUser);
            return HttpStatus.OK;
        }
        routeByClientId.setAddedByUser(Utils.getCurrentMillis());
        routesByUser.add(routeByClientId);
        actualizeUsersRouteList(byUsername, routesByUser);
        return HttpStatus.OK;
    }

}
