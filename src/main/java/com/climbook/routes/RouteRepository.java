package com.climbook.routes;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RouteRepository extends CrudRepository<Route, String> {
    Route findRouteByClientId(String clientId);

    List<Route> findRoutesByRock_ClientId(String clientId);
}


