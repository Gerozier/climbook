package com.climbook.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class RouteDTO {

    private String clientId;

    private String name;
    private String description;
    private String difficulty;
    private boolean complete;
    private long createDate;
    private long lastUpdate;

}
