package com.climbook.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class RegisterDTO {

    private String username;
    private String password1;
    private String password2;
    private String email;

}
