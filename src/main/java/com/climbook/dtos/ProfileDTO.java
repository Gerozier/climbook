package com.climbook.dtos;

import com.climbook.rocks.Rock;
import com.climbook.routes.Route;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
@Builder
public class ProfileDTO {

    private final String username;
    private final String email;
    private final List<Route> routes;
    private final List<Rock> rocks;
}
