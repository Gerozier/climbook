package com.climbook.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class RockDTO {

    private String clientId;

    private String description;
    private boolean complete;
    private String name;
    private long createDate;
    private long lastUpdate;

}
