package com.climbook.user;

import com.climbook.dtos.ProfileDTO;
import com.climbook.dtos.RegisterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements UserDetailsService {

    // Fields

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    // Constructor


    // Methods

    public boolean usernameExists(String username) {
        return userRepository.existsByUsername(username);
    }

    public User registerUser(RegisterDTO registerDto, Role role) {
        if (usernameExists(registerDto.getUsername())) {
            return null;
        }
        String encPassword = passwordEncoder.encode(registerDto.getPassword1());
        User registeredUser = userRepository.save(new User(registerDto.getUsername(), encPassword, role, registerDto.getEmail()));
        return registeredUser;
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        UserBuilder builder;
        if (user != null) {
            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.password(user.getPassword());
            builder.roles(user.getRole().toString());
        } else {
            throw new UsernameNotFoundException("Nutzer nicht gefunden");
        }
        return builder.build();
    }

    public ProfileDTO getUserProfile(String username) throws UsernameNotFoundException {
        User actualUser = userRepository.findByUsername(username);
        ProfileDTO profileData = ProfileDTO.builder()
                .email(actualUser.getEmail())
                .username(actualUser.getUsername())
                .rocks(actualUser.getRocks())
                .routes(actualUser.getRoutes())
                .build();
        return profileData;
    }

    public List<User> getAllUsers() {
        List<User> all = (List<User>) userRepository.findAll();
        return all;
    }

    public User changeUserRole(String username, String role) {
        if (!usernameExists(username)) {
            return null;
        }
        Role newRole = null;
        if (role.equals(Role.ADMIN.toString())) {
            newRole = Role.ADMIN;
        }

        if (role.equals(Role.USER.toString())) {
            newRole = Role.USER;
        }
        if (newRole == null) {
            return null;
        }

        User byUsername = userRepository.findByUsername(username);
        byUsername.setRole(newRole);

        User updatedUser = userRepository.save(byUsername);
        return updatedUser;
    }

    public HttpStatus deleteUser(String username, String sessionUserName) {
        if (sessionUserName.equals(username) || !usernameExists(username)) {
            return null;
        }
        User byUsername = userRepository.findByUsername(username);
        userRepository.delete(byUsername);
        return HttpStatus.OK;
    }

}




