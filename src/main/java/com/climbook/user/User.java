package com.climbook.user;

import com.climbook.rocks.Rock;
import com.climbook.routes.Route;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Data
public class User {

    @Id
    @GeneratedValue
    private int userId;

    private String username;

    private String password;

    private Role role;

    private String email;

    @ManyToMany
    private List<Route> routes;

    @ManyToMany
    private List<Rock> rocks;

    public User() {
    }

    public User(String username, String password, Role role, String email) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
    }

}
