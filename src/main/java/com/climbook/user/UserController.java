package com.climbook.user;

import com.climbook.MailValidator;
import com.climbook.dtos.ProfileDTO;
import com.climbook.dtos.RegisterDTO;
import com.climbook.dtos.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

    public static final String API_USER = "/api/v1/user";
    public static final String API_USER_PROFILE = "/api/v1/user/profile";

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    private boolean checkRoleAdmin(UserDetails userDetails) {
        return !userDetails.getAuthorities().toString().equals("[ROLE_ADMIN]");
    }

    @GetMapping(API_USER)
    public ResponseEntity<List<UserDTO>> getAllUsers(@AuthenticationPrincipal UserDetails userDetails) {
        if (checkRoleAdmin(userDetails)) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .build();
        }
        List<User> allUsers = userService.getAllUsers();
        List<UserDTO> userDTOList = new ArrayList<>();
        allUsers.forEach(user -> {
            UserDTO userDTO = new UserDTO(user.getUsername(), user.getRole());
            userDTOList.add(userDTO);
        });
        return ResponseEntity.ok(userDTOList);
    }

    @PutMapping(API_USER)
    public ResponseEntity<HttpStatus> changeUserRole(@AuthenticationPrincipal UserDetails userDetails,
                                               @RequestParam String username,
                                               @RequestParam String role) {
        if (checkRoleAdmin(userDetails)) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .build();
        }
        User updatedUser = userService.changeUserRole(username, role);
        if (updatedUser == null) {
            return ResponseEntity
                    .badRequest()
                    .build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(API_USER)
    public ResponseEntity<HttpStatus> deleteUser(@AuthenticationPrincipal UserDetails userDetails,
                                                 @RequestParam String username) {
        if (checkRoleAdmin(userDetails)) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .build();
        }
        HttpStatus httpStatus = userService.deleteUser(username, userDetails.getUsername());
        if (httpStatus == null) {
            return ResponseEntity
                    .badRequest()
                    .build();
        }
        return ResponseEntity
                .status(httpStatus)
                .build();
    }

    @PostMapping("/api/v1/register/user")
    public ResponseEntity<HttpStatus> registerUser(@RequestBody RegisterDTO registerDto) {
        if (!registerDto.getPassword1().equals(registerDto.getPassword2())) {
            return ResponseEntity
                    .badRequest()
                    .build();
        }
        if (!MailValidator.emailValidation(registerDto.getEmail())) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .build();
        }
        User registeredUser = userService.registerUser(registerDto, Role.USER);
        if (registeredUser == null) { // username exists
            return ResponseEntity
                    .status(HttpStatus.NOT_ACCEPTABLE)
                    .build();
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(API_USER_PROFILE)
    public ResponseEntity<ProfileDTO> getUserProfile(@AuthenticationPrincipal UserDetails userDetails) {
        ProfileDTO userProfile = userService.getUserProfile(userDetails.getUsername());
        if (userProfile == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        return ResponseEntity.ok(userProfile);
    }

}
