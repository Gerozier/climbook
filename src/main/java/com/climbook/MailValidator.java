package com.climbook;

import org.apache.commons.validator.routines.EmailValidator;

public class MailValidator {

    public static boolean emailValidation(String email) {
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(email);
    }
}
