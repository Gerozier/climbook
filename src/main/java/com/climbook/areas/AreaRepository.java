package com.climbook.areas;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaRepository extends CrudRepository<Area, String> {
    Area findAreaByClientId(String clientId);
}
