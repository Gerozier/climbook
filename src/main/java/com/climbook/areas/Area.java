package com.climbook.areas;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@Builder
public class Area {

    @Id
    @JsonIgnore
    private String areaId;
    private String clientId;

    private String name;
    private String description;

    private long createDate;
    private long lastUpdate;


    public Area() {
    }

}
