package com.climbook.areas;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AreaController {

    public static final String PATH_API_AREAS = "/api/v1/areas";
    public static final String PATH_API_AREAS_CLIENT_ID = "/api/v1/areas/{clientId}";
    public static final String PATH_API_UNREGISTERED_AREAS_CLIENT_ID = "/api/v1/unregistered/areas/{clientId}";

    private final AreaService areaService;

    public AreaController(AreaService areaService) {
        this.areaService = areaService;
    }

    @GetMapping(PATH_API_AREAS)
    public ResponseEntity<List<Area>> getAllAreas() {
        List<Area> allAreas = areaService.getAllAreas();
        if (allAreas.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.NO_CONTENT)
                    .body(allAreas);
        }
        return ResponseEntity.ok(allAreas);
    }

    @GetMapping(PATH_API_AREAS_CLIENT_ID)
    public ResponseEntity<Area> getArea(@PathVariable String clientId) {
        Area areaByClientId = areaService.getOneArea(clientId);
        if (areaByClientId == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(areaByClientId);
    }

    @PostMapping("/api/v1/create/areas")
    public ResponseEntity<HttpStatus> createArea(@RequestBody Area area) {
        if (area.getName().isBlank() || StringUtils.isEmpty(area.getName())) {
            return ResponseEntity.badRequest().build();
        }
        areaService.createArea(area);

        return ResponseEntity.ok().build();
    }

    @PutMapping(PATH_API_AREAS_CLIENT_ID)
    public ResponseEntity<HttpStatus> updateArea(@RequestBody Area area,
                                           @PathVariable String clientId) {
        if (area.getName().isBlank() || StringUtils.isEmpty(area.getName())) {
            return ResponseEntity.badRequest().build();
        }
        HttpStatus httpStatus = areaService.updateArea(area, clientId);
        if (httpStatus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(PATH_API_AREAS_CLIENT_ID)
    public ResponseEntity<HttpStatus> deleteArea(@PathVariable String clientId) {
        HttpStatus httpStatus = areaService.deleteArea(clientId);
        if (httpStatus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(httpStatus).build();
    }

    @GetMapping(PATH_API_UNREGISTERED_AREAS_CLIENT_ID)
    public ResponseEntity<Area> getAreaUnregistered(@PathVariable String clientId) {
        Area areaByClientId = areaService.getOneArea(clientId);
        if (areaByClientId == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(areaByClientId);
    }

}
