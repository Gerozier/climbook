package com.climbook.areas;


import com.climbook.Utils;
import com.climbook.rocks.Rock;
import com.climbook.rocks.RockRepository;
import com.climbook.routes.Route;
import com.climbook.routes.RouteRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AreaService {

    private final AreaRepository areaRepository;
    private final RockRepository rockRepository;
    private final RouteRepository routeRepository;

    public AreaService(AreaRepository areaRepository, RockRepository rockRepository, RouteRepository routeRepository) {
        this.areaRepository = areaRepository;
        this.rockRepository = rockRepository;
        this.routeRepository = routeRepository;
    }


    public List<Area> getAllAreas() {
        List<Area> allAreas = (List<Area>) areaRepository.findAll();
        return allAreas;
    }

    public Area getOneArea(String clientId) {
        Area areaByClientId = areaRepository.findAreaByClientId(clientId);
        return areaByClientId;
    }

    public void createArea(Area area) {
        Area save = Area.builder()
                .areaId(UUID.randomUUID().toString())
                .clientId(area.getClientId())
                .description(area.getDescription())
                .name(area.getName())
                .createDate(Utils.getCurrentMillis())
                .build();
        areaRepository.save(save);
    }

    public HttpStatus updateArea(Area area, String clientId) {
        Area areaToUpdateByClientId = areaRepository.findAreaByClientId(clientId);
        if (areaToUpdateByClientId == null) {
            return null;
        }

        areaToUpdateByClientId.setName(area.getName());
        areaToUpdateByClientId.setDescription(area.getDescription());
        areaToUpdateByClientId.setLastUpdate(Utils.getCurrentMillis());

        areaRepository.save(areaToUpdateByClientId);

        return HttpStatus.OK;
    }

    public HttpStatus deleteArea(String clientId) {
        Area areaToDelete = areaRepository.findAreaByClientId(clientId);
        if (areaToDelete == null) {
            return null;
        }
        List<Rock> rocksByArea_clientId = rockRepository.findRocksByArea_ClientId(areaToDelete.getClientId());

        rocksByArea_clientId.forEach(rock -> {
            List<Route> routesByRock_clientId = routeRepository.findRoutesByRock_ClientId(rock.getClientId());
            routesByRock_clientId.forEach(routeRepository::delete);
            rockRepository.delete(rock);
        });

        areaRepository.delete(areaToDelete);

        return HttpStatus.OK;
    }

}
