package com.climbook.rocks;

import com.climbook.dtos.RockDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RockController {

    public static final String PATH_API_ROCKS_CLIENT_ID = "/api/v1/rocks/{clientId}";
    public static final String PATH_API_UNREGISTERED_ROCKS = "/api/v1/unregistered/rocks/{clientId}";
    public static final String PATH_API_AREA_ROCKS_AREA_CLIENT_ID = "/api/v1/area/rocks/{areaClientId}";

    private final RockService rockService;

    public RockController(RockService rockService) {
        this.rockService = rockService;
    }

    @GetMapping(PATH_API_AREA_ROCKS_AREA_CLIENT_ID)
    public ResponseEntity<List<RockDTO>> getAllRocksByAreaClientId(@PathVariable String areaClientId, @AuthenticationPrincipal UserDetails userDetails) {
        List<RockDTO> allRocks = rockService.getAllRocksByAreaClientId(areaClientId, userDetails);
        if (allRocks.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.NO_CONTENT)
                    .body(allRocks);
        }
        return ResponseEntity.ok(allRocks);
    }

    @GetMapping(PATH_API_ROCKS_CLIENT_ID)
    public ResponseEntity<RockDTO> getRock(@PathVariable String clientId, @AuthenticationPrincipal UserDetails userDetails) {
        RockDTO rockByClientId = rockService.getOneRock(clientId, userDetails);
        if (rockByClientId == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(rockByClientId);
    }

    @PostMapping(PATH_API_AREA_ROCKS_AREA_CLIENT_ID)
    public ResponseEntity<Rock> createRock(@RequestBody Rock rock, @PathVariable String areaClientId) {
        if (StringUtils.isEmpty(rock.getName()) || rock.getName().isBlank()) {
            return ResponseEntity.badRequest().build();
        }
        Rock rockToCreate = rockService.createRock(rock, areaClientId);
        return ResponseEntity.ok(rockToCreate);
    }

    @PutMapping(PATH_API_ROCKS_CLIENT_ID)
    public ResponseEntity<Rock> updateRock(@RequestBody Rock rock, @PathVariable String clientId) {
        if (StringUtils.isEmpty(rock.getName())) {
            return ResponseEntity.badRequest().build();
        }
        Rock updatedRock = rockService.updateRock(rock, clientId);
        if (updatedRock == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedRock);
    }

    @DeleteMapping(PATH_API_ROCKS_CLIENT_ID)
    public ResponseEntity<HttpStatus> deleteRock(@PathVariable String clientId) {
        HttpStatus httpStatus = rockService.deleteRock(clientId);
        if (httpStatus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(httpStatus).build();
    }

    @PostMapping(PATH_API_ROCKS_CLIENT_ID)
    public ResponseEntity<HttpStatus> saveRockToUser(@PathVariable String clientId, @AuthenticationPrincipal UserDetails userDetails) {
        HttpStatus httpStatus = rockService.setOrDeleteRockFromUserList(clientId, userDetails);
        if (httpStatus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }


    @GetMapping(PATH_API_UNREGISTERED_ROCKS)
    public ResponseEntity<RockDTO> getRockUnregistered(@PathVariable String clientId, @AuthenticationPrincipal UserDetails userDetails) {
        RockDTO rockByClientId = rockService.getOneRock(clientId, userDetails);
        if (rockByClientId == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(rockByClientId);
    }

    @GetMapping("/api/v1/unregistered/area/rocks/{areaClientId}")
    public ResponseEntity<List<RockDTO>> getAllRocksByAreaClientIdUnregistered(@PathVariable String areaClientId, @AuthenticationPrincipal UserDetails userDetails) {
        List<RockDTO> allRocks = rockService.getAllRocksByAreaClientId(areaClientId, userDetails);
        if (allRocks.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.NO_CONTENT)
                    .build();
        }
        return ResponseEntity.ok(allRocks);
    }
}
