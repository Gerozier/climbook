package com.climbook.rocks;

import com.climbook.Utils;
import com.climbook.areas.AreaRepository;
import com.climbook.dtos.RockDTO;
import com.climbook.routes.Route;
import com.climbook.routes.RouteRepository;
import com.climbook.user.User;
import com.climbook.user.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Service
public class RockService {

    private final RockRepository rockRepository;
    private final RouteRepository routeRepository;
    private final AreaRepository areaRepository;
    private final UserRepository userRepository;

    public RockService(AreaRepository areaRepository, RockRepository rockRepository, RouteRepository routeRepository, UserRepository userRepository) {
        this.rockRepository = rockRepository;
        this.routeRepository = routeRepository;
        this.areaRepository = areaRepository;
        this.userRepository = userRepository;
    }

    private RockDTO buildRockDTO(Rock rock) {
        return RockDTO.builder()
                .clientId(rock.getClientId())
                .createDate(rock.getCreateDate())
                .complete(false)
                .name(rock.getName())
                .description(rock.getDescription())
                .build();
    }

    private void actualizeUsersRockList(User byUsername, List<Rock> rocksByUser) {
        byUsername.setRocks(rocksByUser);
        userRepository.save(byUsername);
    }

    public List<RockDTO> getAllRocksByAreaClientId(String areaClientId, UserDetails userDetails) {
        List<Rock> rocksByAreaClientId = rockRepository.findRocksByArea_ClientId(areaClientId);
        rocksByAreaClientId.sort(Comparator.comparing(Rock::getName));
        if (userDetails == null) {
            List<RockDTO> unregisteredUserList = new ArrayList<>();
            if (!rocksByAreaClientId.isEmpty()) {
                unregisteredUserList.add(buildRockDTO(rocksByAreaClientId.get(0)));
            }
            return unregisteredUserList;
        }

        List<RockDTO> rockDTOList = new ArrayList<>();
        rocksByAreaClientId.forEach(rock -> rockDTOList.add(buildRockDTO(rock)));
        List<Rock> rocks = rockRepository.findRocksByUserName(userDetails.getUsername());
        rocks.forEach(rock -> rockDTOList.forEach(rockDTO -> {
            if (rockDTO.getName().equals(rock.getName())) {
                rockDTO.setComplete(true);
            }
        }));

        return rockDTOList;
    }

    public RockDTO getOneRock(String clientId, UserDetails userDetails) {
        Rock rockByClientId = rockRepository.findRockByClientId(clientId);
        if (rockByClientId == null) {
            return null;
        }
        RockDTO rockDTO = buildRockDTO(rockByClientId);
        return rockDTO;
    }

    public Rock createRock(Rock rock, String areaClientId) {
        Rock rockToSave = rockRepository.save(
                Rock.builder()
                        .area(areaRepository.findAreaByClientId(areaClientId))
                        .rockId(UUID.randomUUID().toString())
                        .clientId(UUID.randomUUID().toString())
                        .description(rock.getDescription())
                        .name(rock.getName())
                        .createDate(Utils.getCurrentMillis())
                        .build());
        return rockToSave;
    }

    public Rock updateRock(Rock rock, String clientId) {
        Rock rockToUpdateByClientId = rockRepository.findRockByClientId(clientId);
        if (rockToUpdateByClientId == null) {
            return null;
        }

        rockToUpdateByClientId.setName(rock.getName());
        rockToUpdateByClientId.setDescription(rock.getDescription());
        rockToUpdateByClientId.setLastUpdate(Utils.getCurrentMillis());

        Rock updatedRock = rockRepository.save(rockToUpdateByClientId);

        return updatedRock;
    }


    public HttpStatus deleteRock(String clientId) {
        Rock rockToDelete = rockRepository.findRockByClientId(clientId);
        if (rockToDelete == null) {
            return null;
        }

        List<Route> routesByRock_clientId = routeRepository.findRoutesByRock_ClientId(clientId);
        routesByRock_clientId.forEach(routeRepository::delete);

        rockRepository.delete(rockToDelete);

        return HttpStatus.OK;
    }

    public HttpStatus setOrDeleteRockFromUserList(String clientId, UserDetails userDetails) {
        Rock rockByClientId = rockRepository.findRockByClientId(clientId);
        if (rockByClientId == null) {
            return null;
        }
        User byUsername = userRepository.findByUsername(userDetails.getUsername());
        List<Rock> rocksByUser = rockRepository.findRocksByUserName(userDetails.getUsername());
        Rock containedRock = rocksByUser.stream()
                .filter(rock -> rock.getClientId().equals(clientId))
                .findAny()
                .orElse(null);
        if (containedRock != null) {
            rocksByUser.remove(containedRock);
            actualizeUsersRockList(byUsername, rocksByUser);
            return HttpStatus.OK;
        }
        rockByClientId.setAddedByUser(Utils.getCurrentMillis());
        rocksByUser.add(rockByClientId);
        actualizeUsersRockList(byUsername, rocksByUser);
        return HttpStatus.OK;
    }

}
