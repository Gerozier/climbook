package com.climbook.rocks;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RockRepository extends CrudRepository<Rock, String> {

    Rock findRockByClientId(String clientId);

    List<Rock> findRocksByArea_ClientId(String clientId);

    @Query("SELECT u.rocks FROM User u WHERE u.username = ?1")
    List<Rock> findRocksByUserName(String username);
}
