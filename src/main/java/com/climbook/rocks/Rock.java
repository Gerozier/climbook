package com.climbook.rocks;

import com.climbook.areas.Area;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@AllArgsConstructor
@Builder
public class Rock {

    @Id
    @JsonIgnore
    private String rockId;

    private String clientId;

    private String description;
    private String name;
    private long createDate;
    private long lastUpdate;
    private long addedByUser;

    @ManyToOne
    private Area area;

    public Rock() {
    }

}
