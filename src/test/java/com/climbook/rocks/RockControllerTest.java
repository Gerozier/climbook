package com.climbook.rocks;

import com.climbook.areas.Area;
import com.climbook.areas.AreaRepository;
import com.climbook.routes.RouteRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RockControllerTest {

    public static final String USER_TEST_ADMIN = "TestAdmin";
    public static final String PASSWORD_ADMIN = "admin";
    public static final String INVALIDCLIENTID = "Invalid ClientID";
    public static final String API_AREA_ROCKS = "/api/v1/area/rocks/";
    public static final String API_ROCKS = "/api/v1/rocks/";

    @LocalServerPort
    private int port;

    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private RockRepository rockRepository;
    @Autowired
    private RouteRepository routeRepository;

    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    private String createUrlWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    public void cleanDb() {
        routeRepository.deleteAll();
        rockRepository.deleteAll();
        areaRepository.deleteAll();
    }

    public void setup() {
        cleanDb();
        Area harz = areaRepository.save(new Area( UUID.randomUUID().toString(), UUID.randomUUID().toString(), "Harz", "", 0, 0));
        rockRepository.save(createRock("Brausenstein", "Geil", harz));
        rockRepository.save(createRock("Topograph", "Noch Geiler", harz));

    }

    private static Rock createRock(String name, String description, Area area) {
        Rock rock = Rock.builder()
                .rockId(UUID.randomUUID().toString())
                .clientId(UUID.randomUUID().toString())
                .name(name)
                .description(description)
                .area(area)
                .build();
        return rock;
    }


    @Test
    @DisplayName("GET /api/v1/area/rocks/{areaClientId} success")
    void getAllRocksByArea_filled_db() {
        setup();
        String clientId = areaRepository.findAll().iterator().next().getClientId();

        ResponseEntity<List> allRocks = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .getForEntity(createUrlWithPort(API_AREA_ROCKS + clientId), List.class);

        assertEquals(HttpStatus.OK, allRocks.getStatusCode());
    }

    @Test
    @DisplayName("GET /api/v1/area/rocks/{areaClientId} no_content")
    void getAllRocksByArea_no_rocks_found() {
        Area harz = areaRepository.save(new Area(UUID.randomUUID().toString(), UUID.randomUUID().toString(), "Harz", "", 0, 0));
        ResponseEntity<List> allRocks = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .getForEntity(createUrlWithPort(API_AREA_ROCKS + harz.getClientId()), List.class);
        assertEquals(HttpStatus.NO_CONTENT, allRocks.getStatusCode());
    }


    @Test
    @DisplayName("GET /api/v1/rocks/{clientId} success")
    void getRock_with_valid_clientID() {
        setup();
        String clientId = rockRepository.findAll().iterator().next().getClientId();
        ResponseEntity<Rock> rock = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .getForEntity(createUrlWithPort(API_ROCKS + clientId), Rock.class);
        assertEquals(HttpStatus.OK, rock.getStatusCode());
    }

    @Test
    @DisplayName("GET /api/v1/rocks/{clientId} not_found")
    void getRock_with_invalid_clientID() {
        setup();
        String clientId = "34578734fshegdyfhdt092";
        ResponseEntity<Rock> rock = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .getForEntity(createUrlWithPort(API_ROCKS + INVALIDCLIENTID), Rock.class);
        assertEquals(HttpStatus.NOT_FOUND, rock.getStatusCode());
    }

    @Test
    @DisplayName("POST /api/v1/area/rocks/{areaClientId} success")
    void createValidRock() {
        ResponseEntity<Rock> rockToCreate = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .postForEntity(createUrlWithPort(API_AREA_ROCKS + "1"), createRock("TestName", "Test",  null), Rock.class);
        assertEquals(HttpStatus.OK, rockToCreate.getStatusCode());
    }

    @Test
    @DisplayName("POST /api/v1/area/rocks/{areaClientId} bad_request")
    void createRockWithoutName() {
        ResponseEntity<Rock> rockToCreate = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .postForEntity(createUrlWithPort(API_AREA_ROCKS + "1"), createRock("", "Test",  null), Rock.class);
        assertEquals(HttpStatus.BAD_REQUEST, rockToCreate.getStatusCode());
    }

    @Test
    @DisplayName("PUT api/v1/rocks/{clientId} success")
    void updateRock() {
        setup();

        String clientId = rockRepository.findAll().iterator().next().getClientId();

        Rock updatedRock = Rock.builder().name("Test").build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Rock> entity = new HttpEntity<>(updatedRock, headers);

        ResponseEntity<Rock> response = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .exchange(createUrlWithPort(API_ROCKS + clientId), HttpMethod.PUT, entity, Rock.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT api/v1/rocks/{clientId} not_found")
    void updateRockNotFound() {
        setup();

        Rock updatedRock = Rock.builder().name("Test").build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Rock> entity = new HttpEntity<>(updatedRock, headers);

        ResponseEntity<Rock> response = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .exchange(createUrlWithPort(API_ROCKS + "1"), HttpMethod.PUT, entity, Rock.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT api/v1/rocks/{clientId} bad_request")
    void updateRockBadRequest() {
        setup();

        String clientId = rockRepository.findAll().iterator().next().getClientId();

        Rock updatedRock = Rock.builder().name("").build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Rock> entity = new HttpEntity<>(updatedRock, headers);

        ResponseEntity<Rock> response = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .exchange(createUrlWithPort(API_ROCKS + clientId), HttpMethod.PUT, entity, Rock.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE /api/v1/rocks/{clientId} success")
    void deleteRock() {
        setup();

        String clientId = rockRepository.findAll().iterator().next().getClientId();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Rock> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Rock> response = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .exchange(createUrlWithPort(API_ROCKS + clientId), HttpMethod.DELETE, entity, Rock.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE /api/v1/rocks/{clientId} not-found")
    void deleteRockNotFound() {
        setup();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Rock> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Rock> response = testRestTemplate
                .withBasicAuth(USER_TEST_ADMIN, PASSWORD_ADMIN)
                .exchange(createUrlWithPort(API_ROCKS + "1"), HttpMethod.DELETE, entity, Rock.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }


}
