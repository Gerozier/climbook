package com.climbook.rocks;

import com.climbook.areas.Area;
import com.climbook.areas.AreaRepository;
import com.climbook.dtos.RockDTO;
import com.climbook.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RockServiceTest {

    @Autowired
    private RockService rockService;
    @Autowired
    private UserService userService;

    @Autowired
    private RockRepository rockRepository;

    @Autowired
    private AreaRepository areaRepository;


    @BeforeEach
    public void init() {
        Area testArea1 = Area.builder().name("TestArea1").areaId("1").clientId("1").build();
        areaRepository.save(testArea1);
        Rock test1 = createRock("Test1", "1", "1", testArea1);
        Rock test2 = createRock("Test2", "2", "2", testArea1);
        rockRepository.save(test1);
        rockRepository.save(test2);

    }

    private Rock createRock(String name, String rockId, String clientId, Area area) {
        return Rock.builder()
                .rockId(rockId)
                .clientId(clientId)
                .name(name)
                .area(area)
                .build();
    }


    @Test
    void getAllRocksByAreaClientIdUnregisteredUser() {
        List<RockDTO> allRocksByAreaClientId = rockService.getAllRocksByAreaClientId("1", null);
        assertThat(allRocksByAreaClientId.size()).isEqualTo(1);
    }

    @Test
    void getAllRocksByAreaClientIdRegisteredUser() {
        UserDetails principal = userService.loadUserByUsername("TestAdmin");
        List<RockDTO> allRocksByAreaClientId = rockService.getAllRocksByAreaClientId("1", principal);
        assertThat(allRocksByAreaClientId.size()).isEqualTo(2);
    }

    @Test
    void getOneRock() {
        RockDTO oneRock = rockService.getOneRock("1", null);
        assertThat(oneRock.getName()).isEqualTo("Test1");
    }

    @Test
    void createRock() {
    }

    @Test
    void updateRock() {
    }

    @Test
    void deleteRock() {
    }

    @Test
    void setRockToUserList() {
    }
}
