package com.climbook.routes;

import com.climbook.areas.Area;
import com.climbook.rocks.Rock;
import com.climbook.rocks.RockRepository;
import com.climbook.user.User;
import com.climbook.user.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RouteControllerTest {

    public static final String API_ROUTES = "/api/v1/routes/";
    public static final String USERNAME = "TestAdmin";
    public static final String PASSWORD = "admin";
    public static final String API_ROCK_ROUTES = "/api/v1/rock/routes/";
    public static final String INVALID_CLIENT_ID = "invalid_clientID";
    @LocalServerPort
    private int port;

    @Autowired
    private RockRepository rockRepository;
    @Autowired
    private RouteRepository routeRepository;
    @Autowired
    private UserRepository userRepository;


    private final TestRestTemplate testRestTemplate = new TestRestTemplate();

    private String createUrlWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    public void cleanDb() {
        routeRepository.deleteAll();
        rockRepository.deleteAll();
    }

    @BeforeEach
    public void setup() {
        cleanDb();

        Rock rock = createRock("Rock_With_Routes");
        rockRepository.save(rock);

        Route route = createRoute("Alter Weg", "Geil", "5", rock);
        Route route1 = createRoute("Neuer Weg", "Noch Geiler", "7",rock);
        Route route2 = createRoute("Zukünftiger Weg", "Noch Geilerer", "1",rock);
        routeRepository.save(route);
        routeRepository.save(route1);
        routeRepository.save(route2);
    }

    private Rock createRock(String name) {
        return Rock.builder()
                .rockId(UUID.randomUUID().toString())
                .clientId(UUID.randomUUID().toString())
                .description("")
                .name(name)
                .area(null)
                .build();
    }

    private static Route createRoute(String name, String description, String difficulty, Rock rock) {
        Route route = Route.builder()
                .routeId(UUID.randomUUID().toString())
                .clientId(UUID.randomUUID().toString())
                .name(name)
                .description(description)
                .difficulty(difficulty)
                .rock(rock)
                .build();
        return route;
    }

    private String getClientId() {
        return routeRepository.findAll().iterator().next().getClientId();
    }

    @Test
    @DisplayName("GET getAllRoutesByRock_filled_db success")
    void getAllRoutesByRock_filled_db() {
        String clientId = rockRepository.findAll().iterator().next().getClientId();
        ResponseEntity<List> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .getForEntity(createUrlWithPort(API_ROCK_ROUTES + clientId), List.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("GET getAllRoutesByRock_no_routes_found no_content")
    void getAllRoutesByRock_no_routes_found() {
        Rock rock = createRock("Rock_Without_Routes");
        rockRepository.save(rock);
        ResponseEntity<List> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .getForEntity(createUrlWithPort(API_ROCK_ROUTES + rock.getClientId()), List.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }


    @Test
    @DisplayName("GET getRoute_with_valid_clientID success")
    void getRoute_with_valid_clientID() {
        String clientId = getClientId();
        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .getForEntity(createUrlWithPort(API_ROUTES + clientId), Route.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("POST createRoute success")
    void createRoute() {
        Route newRoute = createRoute("TestName", "Test", "test1", null);
        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .postForEntity(createUrlWithPort(API_ROCK_ROUTES + "1"), newRoute, Route.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("POST createRoute_without_name bad_request")
    void createRoute_without_name() {
        Route newRoute = createRoute("", "Test", "test1", null);
        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .postForEntity(createUrlWithPort(API_ROCK_ROUTES + "1"), newRoute, Route.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT updateRoute success")
    void updateRoute() {
        String clientId = getClientId();

        Route updateForRoute = Route.builder()
                .name("Update")
                .difficulty("")
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Route> entity = new HttpEntity<>(updateForRoute, headers);

        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(API_ROUTES + clientId), HttpMethod.PUT, entity, Route.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT updateRoute_not_found not_found")
    void updateRoute_not_found() {
        Route updateForRoute = Route.builder()
                .name("Update")
                .difficulty("")
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Route> entity = new HttpEntity<>(updateForRoute, headers);

        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(API_ROUTES + INVALID_CLIENT_ID), HttpMethod.PUT, entity, Route.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT updateRoute_bad_request bad_request")
    void updateRoute_bad_request() {
        String clientId = getClientId();

        Route updateForRoute = Route.builder()
                .name("")
                .difficulty("")
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Route> entity = new HttpEntity<>(updateForRoute, headers);

        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(API_ROUTES + clientId), HttpMethod.PUT, entity, Route.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE deleteArea success")
    void deleteArea() {
        String clientId = getClientId();


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Route> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(API_ROUTES + clientId), HttpMethod.DELETE, entity, Route.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE deleteArea_not_found not_found")
    void deleteArea_not_found() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Area> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Route> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(API_ROUTES + INVALID_CLIENT_ID), HttpMethod.DELETE, entity, Route.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    @DisplayName("POST saveRouteToUser success")
    void saveRouteToUser() {
        String clientId = getClientId();
        ResponseEntity<List> response = testRestTemplate
                .withBasicAuth("TestUser", "user")
                .postForEntity(createUrlWithPort(API_ROUTES + clientId), null, List.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        User testUser = userRepository.findByUsername("TestUser");
        userRepository.delete(testUser);
    }

    @Test
    @DisplayName("POST saveRouteToUser_with_invalid_id not_found")
    void saveRouteToUser_with_invalid_id() {
        String clientId = "Invalid_ID";
        ResponseEntity<List> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .postForEntity(createUrlWithPort(API_ROUTES + clientId), null, List.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
