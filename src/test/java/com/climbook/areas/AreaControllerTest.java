package com.climbook.areas;

import com.climbook.rocks.RockRepository;
import com.climbook.routes.RouteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AreaControllerTest {

    public static final String USERNAME = "TestAdmin";
    public static final String PASSWORD = "admin";
    public static final String INVALIDCLIENTID = "Invalid ClientID";
    public static final String AREASPATH = "/api/v1/areas/";
    public static final String CREATE_AREASPATH = "/api/v1/create/areas";

    @LocalServerPort
    private int port;

    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private RockRepository rockRepository;
    @Autowired
    private RouteRepository routeRepository;

    private final TestRestTemplate testRestTemplate = new TestRestTemplate();

    private String createUrlWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    public void cleanDb() {
        routeRepository.deleteAll();
        rockRepository.deleteAll();
        areaRepository.deleteAll();
    }

    private static Area createArea(String name, String description) {
        return Area.builder()
                .areaId(UUID.randomUUID().toString())
                .clientId(UUID.randomUUID().toString())
                .name(name)
                .description(description)
                .build();
    }

    private HttpEntity<Area> getAreaHttpEntity(Area o) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(o, headers);
    }

    private String getClientFromFirstArea() {
        return areaRepository.findAll()
                .iterator()
                .next()
                .getClientId();
    }

    private Area createAreaToUpdate(String name) {
        return Area.builder()
                .name(name)
                .build();
    }

    @BeforeEach
    public void setup() {
        cleanDb();
        Area area = createArea("Gebiet 1", "Beschreibung 1");
        Area area1 = createArea("Gebiet 2", "Beschreibung 2");
        areaRepository.save(area);
        areaRepository.save(area1);
    }

    @Test
    @DisplayName("GET /api/v1/areas/ success")
    void getAllAreas() {
        ResponseEntity<List> allAreas = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .getForEntity(createUrlWithPort(AREASPATH), List.class);
        assertEquals(HttpStatus.OK, allAreas.getStatusCode());
    }

    @Test
    @DisplayName("GET /api/v1/areas/ no_content")
    void getAllAreas_no_areas_found() {
        cleanDb();
        ResponseEntity<List> allAreas = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .getForEntity(createUrlWithPort(AREASPATH), List.class);
        assertEquals(HttpStatus.NO_CONTENT, allAreas.getStatusCode());
    }


    @Test
    @DisplayName("GET /api/v1/areas/ success")
    void getArea() {
        String clientId = getClientFromFirstArea();
        ResponseEntity<Area> area = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .getForEntity(createUrlWithPort(AREASPATH + clientId), Area.class);
        assertEquals(HttpStatus.OK, area.getStatusCode());
    }

    @Test
    @DisplayName("GET /api/v1/areas/{clientId} not_found")
    void getArea_with_invalid_clientID() {
        ResponseEntity<Area> area = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .getForEntity(createUrlWithPort(AREASPATH + INVALIDCLIENTID), Area.class);
        assertEquals(HttpStatus.NOT_FOUND, area.getStatusCode());
    }


    @Test
    @DisplayName("POST createArea success")
    void createArea() {
        Area validAreaToPost = createArea("Test", "Test");
        ResponseEntity<Area> areaToCreate = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .postForEntity(createUrlWithPort(CREATE_AREASPATH), validAreaToPost, Area.class);
        assertEquals(HttpStatus.OK, areaToCreate.getStatusCode());
    }

    @Test
    @DisplayName("POST createArea_without_name bad_request")
    void createArea_without_name() {
        Area invalidAreaToPost = createArea("", "Test");
        ResponseEntity<Area> areaToCreate = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .postForEntity(createUrlWithPort(CREATE_AREASPATH), invalidAreaToPost, Area.class);
        assertEquals(HttpStatus.BAD_REQUEST, areaToCreate.getStatusCode());
    }

    @Test
    @DisplayName("PUT /api/v1/areas/{clientId} success")
    void updateArea() {
        String clientId = getClientFromFirstArea();
        Area areaToUpdate = createAreaToUpdate("Test");
        HttpEntity<Area> entity = getAreaHttpEntity(areaToUpdate);
        ResponseEntity<Area> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(AREASPATH + clientId), HttpMethod.PUT, entity, Area.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT /api/v1/areas/{clientId} not_found")
    void updateArea_not_found() {
        Area areaToUpdate = createAreaToUpdate("Test");
        HttpEntity<Area> entity = getAreaHttpEntity(areaToUpdate);
        ResponseEntity<Area> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(AREASPATH + INVALIDCLIENTID), HttpMethod.PUT, entity, Area.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT /api/v1/areas/{clientId} bad_request")
    void updateArea_bad_request() {
        String clientId = getClientFromFirstArea();
        Area areaToUpdate = createAreaToUpdate("");
        HttpEntity<Area> entity = getAreaHttpEntity(areaToUpdate);
        ResponseEntity<Area> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(AREASPATH + clientId), HttpMethod.PUT, entity, Area.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE /api/v1/areas/{clientId} success")
    void deleteArea() {
        String clientId = getClientFromFirstArea();
        HttpEntity<Area> entity = getAreaHttpEntity(null);
        ResponseEntity<Area> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(AREASPATH + clientId), HttpMethod.DELETE, entity, Area.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE /api/v1/areas/{clientId} not_found")
    void deleteArea_not_found() {
        HttpEntity<Area> entity = getAreaHttpEntity(null);
        ResponseEntity<Area> response = testRestTemplate
                .withBasicAuth(USERNAME, PASSWORD)
                .exchange(createUrlWithPort(AREASPATH + INVALIDCLIENTID), HttpMethod.DELETE, entity, Area.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
