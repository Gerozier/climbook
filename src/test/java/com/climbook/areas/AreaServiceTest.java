package com.climbook.areas;

import com.climbook.rocks.Rock;
import com.climbook.rocks.RockRepository;
import com.climbook.routes.Route;
import com.climbook.routes.RouteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AreaServiceTest {

    @Autowired
    private AreaService areaService;

    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private RockRepository rockRepository;
    @Autowired
    private RouteRepository routeRepository;

    @BeforeEach
    public void init() {
        areaRepository.deleteAll();
        Area area1 = createArea("Name 1", "1", "1", "Description 1");
        Area area2 = createArea("Name 2", "2", "2", "Description 2");
        areaRepository.save(area1);
        areaRepository.save(area2);

    }

    private Area createArea(String name, String areaId, String clientId, String description) {
        return Area.builder()
                .areaId(areaId)
                .clientId(clientId)
                .name(name)
                .description(description)
                .build();
    }

    private void createAreaWithRockAndRoute() {
        Area area4 = createArea("Name 4", "4", "4", "Description 4");
        areaRepository.save(area4);
        Rock rock = Rock.builder()
                .area(area4)
                .rockId("1")
                .clientId("1")
                .name("Name 1")
                .description("Description 1")
                .build();
        rockRepository.save(rock);
        Route route = Route.builder()
                .rock(rock)
                .routeId("1")
                .clientId("1")
                .name("Name 1")
                .difficulty("")
                .description("Description 1")
                .build();
        routeRepository.save(route);
    }

    @Test
    @DisplayName("getAllAreas Filled_DB")
    void getAllAreas() {
        List<Area> allAreas = areaService.getAllAreas();
        assertThat(allAreas.size()).isEqualTo(2);
    }

    @Test
    @DisplayName("getAllAreas Empty_DB")
    void getAllAreasNoData() {
        areaRepository.deleteAll();
        List<Area> allAreas = areaService.getAllAreas();
        assertThat(allAreas.isEmpty());
    }

    @Test
    @DisplayName("getOneArea Valid_Id")
    void getOneAreaByValidClientId() {
        Area oneArea = areaService.getOneArea("1");
        assertThat(oneArea.getName()).isEqualTo("Name 1");
    }

    @Test
    @DisplayName("getOneArea Invalid_Id")
    void getOneAreaByInvalidClientId() {
        Area oneArea = areaService.getOneArea("Invalid_Id");
        assertThat(oneArea).isNull();
    }

    @Test
    @DisplayName("createArea Valid Area")
    void createArea() {
        Area areaToCreate = createArea("Name 3", "3", "3", "Description 3");
        areaService.createArea(areaToCreate);
        Area areaByClientId = areaRepository.findAreaByClientId("3");
        assertThat(areaByClientId.getName()).isEqualTo(areaToCreate.getName());
    }

    @Test
    @DisplayName("updateArea Valid Area/ClientId")
    void updateArea() {
        Area areaToUpdate = Area.builder()
                .name("Update 1")
                .description("Update Description 1")
                .build();
        HttpStatus httpStatus = areaService.updateArea(areaToUpdate, "1");
        assertThat(httpStatus.value()).isEqualTo(200);
    }

    @Test
    @DisplayName("updateArea Valid Area/Invalid ClientId")
    void updateAreaInvalid() {
        Area areaToUpdate = Area.builder()
                .name("Update 1")
                .description("Update Description 1")
                .build();
        HttpStatus httpStatus = areaService.updateArea(areaToUpdate, "Invalid_Id");
        assertThat(httpStatus).isNull();
    }

    @Test
    @DisplayName("deleteArea Valid ClientId")
    void deleteArea() {
        HttpStatus httpStatus = areaService.deleteArea("1");
        assertThat(httpStatus.value()).isEqualTo(200);
    }

    @Test
    @DisplayName("deleteArea Invalid ClientId")
    void deleteAreaInvalid() {
        HttpStatus httpStatus = areaService.deleteArea("Invalid_Id");
        assertThat(httpStatus).isNull();
    }

    @Test
    @DisplayName("deleteArea Valid ClientId with Rock and Routes")
    void deleteAreaComplete() {
        createAreaWithRockAndRoute();
        HttpStatus httpStatus = areaService.deleteArea("4");
        assertThat(httpStatus.value()).isEqualTo(200);
    }
}
