package com.climbook.user;

import com.climbook.dtos.RegisterDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    public void init() {
        userRepository.save(new User("TestUser1", "test", Role.USER, "test@test.de"));
        userRepository.save(new User("TestAdmin1", "test", Role.ADMIN, "test@test.de"));
    }

    @Test
    void usernameExists() {
        boolean exists = userService.usernameExists("TestUser1");
        assertTrue(exists);
    }

    @Test
    void usernameNotExists() {
        boolean exists = userService.usernameExists("Test");
        assertFalse(exists);
    }

    @Test
    void registerUser() {
        User newRegisteredUser = userService.registerUser(new RegisterDTO("TestUser2", "test", "test", "test@test.de"), Role.USER);
        assertThat(newRegisteredUser.getUsername()).isEqualTo("TestUser2");
    }

    @Test
    void registerUserUsernameExists() {
        User newRegisteredUser = userService.registerUser(new RegisterDTO("TestUser1", "test", "test", "test@test.de"), Role.USER);
        assertThat(newRegisteredUser).isEqualTo(null);
    }

//    @Test
//    void loadUserByUsername() {
//        UserDetails userDetails = userService.loadUserByUsername("TestUser");
//        assertThat(userDetails.getUsername()).isEqualTo("TestUser");
//        assertThat(userDetails.getPassword()).isEqualTo("test");
//    }

    @Test
    void loadUserByUsernameNotFound() {
        assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername("TestUser3"));
    }

    @Test
    void getAllUsers() {
        List<User> all = userService.getAllUsers();
        assertThat(all).isNotEmpty();
    }

//    @Test
//    void changeUserRole() {
//        User changeUserRole = userService.changeUserRole("TestUser1", "ADMIN");
//        User changeUserRole2 = userService.changeUserRole("TestAdmin1", "USER");
//        assertThat(changeUserRole.getRole()).isEqualTo(Role.ADMIN);
//        assertThat(changeUserRole2.getRole()).isEqualTo(Role.USER);
//    }

    @Test
    void changeUserRoleNoUserFound() {
        User changeUserRole = userService.changeUserRole("TestUser3", "ADMIN");
        assertThat(changeUserRole).isNull();
    }

    @Test
    void changeUserRoleNoRole() {
        User changeUserRole = userService.changeUserRole("TestUser1", "");
        assertThat(changeUserRole).isNull();
    }

//    @Test
//    void deleteUser() {
//        HttpStatus httpStatus = userService.deleteUser("TestUser1", "FakeUser");
//        assertThat(httpStatus.value()).isEqualTo(200);
//    }

    @Test
    void deleteUserNotExists() {
        HttpStatus httpStatus = userService.deleteUser("TestUser3", "FakeUser");
        assertThat(httpStatus).isNull();
    }

    @Test
    void deleteUserHimself() {
        HttpStatus httpStatus = userService.deleteUser("TestUser1", "TestUser1");
        assertThat(httpStatus).isNull();
    }
}
