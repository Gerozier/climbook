package com.climbook.user;

import com.climbook.dtos.RegisterDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTest {


    public static final String USERNAME_ADMIN = "TestAdmin";
    public static final String PASSWORD_ADMIN = "admin";
    public static final String USERNAME_USER = "TestUser";
    public static final String PASSWORD_USER = "user";
    public static final String API_USER_PATH = "/api/v1/user";
    public static final String API_REGISTER_USER_PATH = "/api/v1/register/user";

    @LocalServerPort
    private int port;


    private final TestRestTemplate testRestTemplate = new TestRestTemplate();

    private String createUrlWithPort() {
        return "http://localhost:" + port;
    }

    @Test
    @DisplayName("GET getAllUsers OK")
    void getAllUsers() {
        ResponseEntity<List> response = testRestTemplate
                .withBasicAuth(USERNAME_ADMIN, PASSWORD_ADMIN)
                .getForEntity(createUrlWithPort() + API_USER_PATH, List.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("GET getAllUsersAsUser UNAUTHORIZED")
    void getAllUsersAsUser() {
        ResponseEntity<List> response = testRestTemplate
                .withBasicAuth(USERNAME_USER, PASSWORD_USER)
                .getForEntity(createUrlWithPort() + API_USER_PATH, List.class);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    @DisplayName("POST registerUser OK")
    void registerUser() {
        RegisterDTO registerTest = RegisterDTO.builder().username("Test").password1("test").password2("test").email("test@test.de").build();
        ResponseEntity<User> response = testRestTemplate
                .postForEntity(createUrlWithPort() + API_REGISTER_USER_PATH, registerTest, User.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("POST registerUserDifferentPasswords BAD_REQUEST")
    void registerUserDifferentPasswords() {
        RegisterDTO registerTest = RegisterDTO.builder().username("Test").password1("Test").password2("test").build();
        ResponseEntity<User> response = testRestTemplate
                .postForEntity(createUrlWithPort() + API_REGISTER_USER_PATH, registerTest, User.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("POST registerUserUsernameExists NOT_ACCEPTABLE")
    void registerUserUsernameExists() {
        RegisterDTO registerTest = RegisterDTO.builder().username("TestAdmin").password1("test").password2("test").email("test@test.de").build();
        ResponseEntity<User> response = testRestTemplate
                .postForEntity(createUrlWithPort() + API_REGISTER_USER_PATH, registerTest, User.class);
        assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT changeUserRole OK")
    void changeUserRole() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort() + API_USER_PATH)
                .queryParam("username", "TestUser")
                .queryParam("role", "USER");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Role> entity = new HttpEntity<>(Role.USER, headers);

        ResponseEntity<User> response = testRestTemplate
                .withBasicAuth(USERNAME_ADMIN, PASSWORD_ADMIN)
                .exchange(builder.build().encode().toUri(), HttpMethod.PUT, entity, User.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT changeUserRoleNoUserFound BAD_REQUEST")
    void changeUserRoleNoUserFound() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort() + API_USER_PATH)
                .queryParam("username", "Quezacotl");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Role> entity = new HttpEntity<>(Role.ADMIN, headers);

        ResponseEntity<User> response = testRestTemplate
                .withBasicAuth(USERNAME_ADMIN, PASSWORD_ADMIN)
                .exchange(builder.build().encode().toUri(), HttpMethod.PUT, entity, User.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("PUT changeUserRoleAsUser UNAUTHORIZED")
    void changeUserRoleAsUser() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort() + API_USER_PATH)
                .queryParam("username", "Quezacotl")
                .queryParam("role", "ADMIN");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Role> entity = new HttpEntity<>(null, headers);

        ResponseEntity<User> response = testRestTemplate
                .withBasicAuth(USERNAME_USER, PASSWORD_USER)
                .exchange(builder.build().encode().toUri(), HttpMethod.PUT, entity, User.class);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE deleteUser OK")
    void deleteUser() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort() + API_USER_PATH)
                .queryParam("username", "Test");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HttpStatus> entity = new HttpEntity<>(null, headers);

        ResponseEntity<HttpStatus> response = testRestTemplate
                .withBasicAuth(USERNAME_ADMIN, PASSWORD_ADMIN)
                .exchange(builder.build().encode().toUri(), HttpMethod.DELETE, entity, HttpStatus.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE deleteUserNoUserFound BAD_REQUEST")
    void deleteUserNoUserFound() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort() + API_USER_PATH)
                .queryParam("username", "Quezacotl");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HttpStatus> entity = new HttpEntity<>(null, headers);

        ResponseEntity<HttpStatus> response = testRestTemplate
                .withBasicAuth(USERNAME_ADMIN, PASSWORD_ADMIN)
                .exchange(builder.build().encode().toUri(), HttpMethod.DELETE, entity, HttpStatus.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("DELETE deleteUserAsUser UNAUTHORIZED")
    void deleteUserAsUser() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort() + API_USER_PATH)
                .queryParam("username", "TestUser");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HttpStatus> entity = new HttpEntity<>(null, headers);

        ResponseEntity<HttpStatus> response = testRestTemplate
                .withBasicAuth(USERNAME_USER, PASSWORD_USER)
                .exchange(builder.build().encode().toUri(), HttpMethod.DELETE, entity, HttpStatus.class);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }
}
